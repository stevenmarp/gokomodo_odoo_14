{
  'name': 'Stevenmarp Technical Odoo Developer',
  'author': 'stevenmarp',
  'version': '1.0.0',
  'depends': [
    'base', 'sale', 'account',
  ],
  'data': [
    # 'security/ir.model.access.csv',
    # 'data/sequence_data.xml',
    # 'data/patient.tag.csv',
    # 'data/patient_tag_data.xml',
    # 'wizard/cancel_appointment.xml',
    # 'views/menu.xml',
    # 'views/patient.xml',
    # 'views/female_patient_view.xml',
    # 'views/appointment_view.xml',
    # 'views/patient_tag.xml',
    # 'views/operations.xml',
    # 'views/odoo_playground.xml',
    # 'views/res_config_settings_views.xml',
      # 'data/update_business_model.xml',
      'views/sale_order_view.xml',

  ],
  'qweb': [
    # 'static/src/xml/nama_widget.xml',
  ],
  'sequence': 1,
  'auto_install': False,
  'installable': True,
  'application': True,
  'category': '-  Odoo Technical V 14',
  'summary': 'Technical odoo 14',
  'license': 'LGPL-3',
  'website': ' https://stevenmarpportofolio.wenikah.com/ ',
  'description': '-'
}
