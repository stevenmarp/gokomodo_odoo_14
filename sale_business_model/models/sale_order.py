# models/sale_order.py

from odoo import models, fields, api

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    business_model = fields.Selection([
        ('retail', 'Pengecer'),
        ('corporate', 'Perusahaan'),
        ('subscription', 'Berlangganan'),
    ], string='Business Model', required=True, default='retail')



    # create: Metode ini menimpa (override) metode create default dari Odoo.
    # Jika business_model tidak ada dalam vals, ia akan diatur ke 'retail'.
    # Menghasilkan nomor urut (sequence) untuk name pesanan penjualan.
    # Memanggil metode create dari superclass untuk membuat rekaman.
    # Setelah pesanan dibuat, menambahkan prefix ke name pesanan berdasarkan business_model:
    # '[RT]' untuk 'retail'
    # '[CORP]' untuk 'corporate'
    # '[SUB]' untuk 'subscription'
    # Mengembalikan hasil dari operasi create.

    @api.model
    def create(self, vals):
        if 'business_model' not in vals:
            vals['business_model'] = 'retail'
        vals['name'] = self.env['ir.sequence'].next_by_code('sale.order') or '/'
        result = super(SaleOrder, self).create(vals)
        if result.name:
            business_model_prefix = {
                'retail': '[RT]',
                'corporate': '[CORP]',
                'subscription': '[SUB]',
            }.get(result.business_model, '')
            result.name = f"{business_model_prefix} - {result.name}"
        return result

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'



    # Jika business_model ada dalam order_id, metode ini akan mengatur tax_id dari sale.order.line berdasarkan nilai business_model:
    # Jika business_model adalah 'retail', akan mencari pajak dengan nama yang mengandung 'Retail Tax'.
    # Jika business_model adalah 'corporate', akan mencari pajak dengan nama yang mengandung 'Corporate Tax'.
    # Jika business_model adalah 'subscription', akan mencari pajak dengan nama yang mengandung 'Subscription Tax'.
    @api.onchange('order_id')
    def _onchange_order_id(self):
        if self.order_id.business_model:
            business_model = self.order_id.business_model
            if business_model == 'retail':
                self.tax_id = self.env['account.tax'].search([('name', 'ilike', 'Retail Tax')])
            elif business_model == 'corporate':
                self.tax_id = self.env['account.tax'].search([('name', 'ilike', 'Corporate Tax')])
            elif business_model == 'subscription':
                self.tax_id = self.env['account.tax'].search([('name', 'ilike', 'Subscription Tax')])
